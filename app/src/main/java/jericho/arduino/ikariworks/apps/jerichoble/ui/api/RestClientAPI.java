package jericho.arduino.ikariworks.apps.jerichoble.ui.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import org.json.JSONObject;

import jericho.arduino.ikariworks.apps.jerichoble.ui.constants.Constants;


/**
 * Created by XyberRnD on 3/13/15.
 */
public class RestClientAPI implements Constants {

    static RestClientAPI instance;
    // Tag used to cancel the request
    String tag_json_obj = "json_obj_req";

   public RestClientAPI(Context context){

   }


    public static RestClientAPI getInstance(Context context) {
        if(instance == null){
            return new RestClientAPI(context);
        }


        return instance;
    }


    public static AsyncHttpClient getHttpClient() {
        return new AsyncHttpClient();
    }

    public static void movement_api(String user_id,
                                    String latitude,
                                    String longitude,
                                    String session_id,
                                    String device_id,

                                    AsyncHttpResponseHandler responseHandler){
        AsyncHttpClient httpClient = getHttpClient();
        String url = movement_api ;
        RequestParams params  = new RequestParams();
        params.add("user_id", user_id);
        params.add("latitude", latitude);
        params.add("longitude",longitude);
        params.add("session_id",session_id);
        params.add("device_id",device_id);
        params.add("device_id",device_id);


        httpClient.get(url,params,responseHandler);

    }



}
